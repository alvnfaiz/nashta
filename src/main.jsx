import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux';
import store from './redux/store';
import App from './App.jsx'
import Layout from './Layout'
import Mahasiswa from './Mahasiswa'
import './index.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

const router = createBrowserRouter([
  {
    element: <Layout />,
    children:[
      {
        path: "/",
        element: <App/>,
      },
      {
        path: "/mahasiswa",
        element: <Mahasiswa/>
      }
    ]
  }
  
]);
const savedState = JSON.parse(localStorage.getItem('reduxState'));


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
if (savedState) {
  store.dispatch({ type: 'HYDRATE_STATE_FROM_LOCAL_STORAGE', payload: savedState });
}