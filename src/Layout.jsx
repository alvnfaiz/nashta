import { Link, Outlet } from "react-router-dom"
import { connect } from 'react-redux';

function Layout({isLoading}){
  return (
    <>
    <nav className='w-screen w-full bg-slate-700 text-white'>
      <div className='flex justify-between max-w-6xl mx-auto'>
        <Link href="/">
          {
            isLoading?"Loading....":"React"
          }
        </Link>
        <div className="flex gap-2">
          <Link to="/">
            Home
          </Link>
          <Link to="/mahasiswa">
            Mahasiswa
          </Link>
        </div>
      </div>
    </nav>
    <Outlet />
    </>
  )
}
const mapStateToProps = (state) => ({
  isLoading: state.mahasiswa.isLoading,
});

export default connect(mapStateToProps)(Layout);