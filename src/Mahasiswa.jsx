import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addMahasiswa, editMahasiswa, hapusMahasiswa } from './redux/actions';

const Mahasiswa = ({ addMahasiswa, editMahasiswa, hapusMahasiswa, mahasiswas, isLoading  }) => {
  const [nama, setNama] = useState('');
  const [umur, setUmur] = useState('');
  const [semester, setSemester] = useState('');
  const [editIndex, setEditIndex] = useState(null);

  const handleTambahMahasiswa = () => {
    addMahasiswa("Mahasiswa " + jumlahMahasiswa, '21', '2');
    setNama();
    setUmur();
    setSemester();
  };

  const handleEditMahasiswa = (index) => {
    const mahasiswaToEdit = mahasiswas[index];
    setNama(mahasiswaToEdit.nama);
    setUmur(mahasiswaToEdit.umur);
    setSemester(mahasiswaToEdit.semester);
    setEditIndex(index);
  };

  const handleUpdateMahasiswa = () => {
    editMahasiswa(editIndex, nama, umur, semester);
    setNama();
    setUmur();
    setSemester();
    setEditIndex(null);
  };

  const handleHapusMahasiswa = (index) => {
    hapusMahasiswa(index);
  };
  const jumlahMahasiswa = mahasiswas.length

  return (
    <div className="container mx-auto p-4">
      <h2 className="text-2xl font-bold mb-4">Daftar Mahasiswa</h2>

      <div className="mt-4">
        
        {editIndex !== null ? (
          <>
          <input
          type="text"
          placeholder="Nama"
          value={nama}
          onChange={(e) => setNama(e.target.value)}
          className="mr-2 border border-gray-400 p-1 rounded"
        />
        <input
          type="text"
          placeholder="Umur"
          value={umur}
          onChange={(e) => setUmur(e.target.value)}
          className="mr-2 border border-gray-400 p-1 rounded"
        />
        <input
          type="text"
          placeholder="Semester"
          value={semester}
          onChange={(e) => setSemester(e.target.value)}
          className="mr-2 border border-gray-400 p-1 rounded"
        />
        <button
            className="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-1 px-2 rounded"
            onClick={handleUpdateMahasiswa}
          >
            Update Mahasiswa
          </button>
          </>
          
        ) : (
          <button
            className="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 rounded"
            onClick={handleTambahMahasiswa}
          >
            Tambah Mahasiswa
          </button>
        )}

      <table className='table-fixed w-full'>
        <thead>
          <tr>
          <td>Nama</td>
          <td>Umur</td>
          <td>Semester</td>
          <td>Aksi</td>
          </tr>
        </thead>
        {mahasiswas.map((mahasiswa, index) => (
          <tr key={index} className="mb-2">
            <td>
            {mahasiswa.nama}
            </td>
            <td>
              {mahasiswa.umur}
            </td>
            <td>
              {mahasiswa.semester}  
            </td>
            <td>
              <button
                className="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded"
                onClick={() => handleEditMahasiswa(index)}
              >
                Edit
              </button>
              <button
                className="ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded"
                onClick={() => handleHapusMahasiswa(index)}
              >
                Hapus
              </button>
            </td>
          </tr>
        ))}
      </table>

      
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  mahasiswas: state.mahasiswa.mahasiswas,
  isLoading: state.mahasiswa.isLoading,
});

export default connect(mapStateToProps, { addMahasiswa, editMahasiswa, hapusMahasiswa })(Mahasiswa);