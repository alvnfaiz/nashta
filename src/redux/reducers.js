import { combineReducers } from 'redux';

// Import your reducers
import mahasiswaReducer from './mahasiswaReducer';

const rootReducer = combineReducers({
  // Add your reducers here
  mahasiswa: mahasiswaReducer,
});

export default rootReducer;