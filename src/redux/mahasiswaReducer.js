import { ADD_MAHASISWA, EDIT_MAHASISWA, HAPUS_MAHASISWA } from './types';

const initialState = {
  mahasiswas: [],
  isLoading: false,
};


const mahasiswaReducer = (state = initialState, action) => {
  
  
  switch (action.type) {
    case ADD_MAHASISWA:
      return {
        ...state,
        mahasiswas: [...state.mahasiswas, action.payload],
      };
    case EDIT_MAHASISWA:
      const updatedMahasiswas = [...state.mahasiswas];
      updatedMahasiswas[action.payload.index] = action.payload;
      return {
        ...state,
        mahasiswas: updatedMahasiswas,
      };
    case HAPUS_MAHASISWA:
      const filteredMahasiswas = state.mahasiswas.filter((_, index) => index !== action.payload.index);
      return {
        ...state,
        mahasiswas: filteredMahasiswas,
      };
    case 'SET_LOADING':
    return {
      ...state,
      isLoading: action.payload,
    };
    default:
      return state;
  }
};

export default mahasiswaReducer;