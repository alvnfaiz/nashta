import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import localStorageMiddleware from './localStorageMiddleware';

const store = createStore(
  rootReducer,
  // Middleware ini akan menyimpan state ke localStorage setiap kali terjadi perubahan state
  applyMiddleware(localStorageMiddleware)
);

export default store;