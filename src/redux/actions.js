import { ADD_MAHASISWA, EDIT_MAHASISWA, HAPUS_MAHASISWA } from './types';

export const addMahasiswa = (nama, umur, semester) => (dispatch) => {
  dispatch({ type: SET_LOADING, payload: true }); // Set isLoading to true

  setTimeout(() => {
    dispatch({
      type: ADD_MAHASISWA,
      payload: { nama, umur, semester },
    });

    dispatch({ type: SET_LOADING, payload: false }); // Set isLoading to false after delay
  }, 5000);
};

export const editMahasiswa = (index, nama, umur, semester) => ({
  type: EDIT_MAHASISWA,
  payload: { index, nama, umur, semester },
});

export const hapusMahasiswa = (index) => ({
  type: HAPUS_MAHASISWA,
  payload: { index },
});