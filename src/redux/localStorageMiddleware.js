const localStorageMiddleware = (store) => (next) => (action) => {
  const result = next(action);
  const { mahasiswa } = store.getState();

  try {
    localStorage.setItem('reduxState', JSON.stringify({ mahasiswa }));
  } catch (error) {
    console.error('Error saving state to localStorage:', error);
  }

  return result;
};

export default localStorageMiddleware;