
import { connect } from 'react-redux';

function App({jumlahMahasiswa}) {

  return (
    <>
      <div className="text-center">
        <p>
          Halaman Home
        </p>
        <p className="read-the-docs">
        Jumlah Mahasiswa : {jumlahMahasiswa}
      </p>
      </div>

    </>
  )
}
const mapStateToProps = (state) => ({
  jumlahMahasiswa: state.mahasiswa.mahasiswas.length,
});

export default connect(mapStateToProps)(App);
